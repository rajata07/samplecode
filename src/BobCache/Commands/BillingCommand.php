<?php
namespace BobCache\Commands;


use BobCache\Models\Customer;
use BobCache\Question\CustomerQuestion;
use BobCache\Services\BillGenerator;
use BobCache\Services\CsvReader;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Formatter\OutputFormatterStyle;
use Symfony\Component\Console\Helper\QuestionHelper;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ChoiceQuestion;
use Symfony\Component\Console\Question\ConfirmationQuestion;

/**
 * Handles generating the bills
 *
 * @author Rajat Arora <rajata07@gmail.com>
 */
class BillingCommand extends Command
{
    const COMMAND_NAME = 'billing';

    const NEW_CUSTOMER_BILL_TEMPLATE = 'newCustomerBillTemplate.odt';

    const EXISTING_CUSTOMER_BILL_TEMPLATE = '_template.odt';

    /**
     * Subscription types available.
     * @var array
     */
    protected $subscriptionTypes = array('basic', 'pro');

    /**
     * @var CsvReader
     */
    protected $csvReader;

    /**
     * @var BillGenerator
     */
    protected $billGenerator;

    /**
     * BillingCommand constructor.
     *
     * @param CsvReader $csvReader CSV Reader Service
     * @param BillGenerator $billCalculator Bill Calculator Service
     */
    public function __construct(CsvReader $csvReader, BillGenerator $billCalculator)
    {
        $this->csvReader = $csvReader;
        $this->billGenerator = $billCalculator;

        parent::__construct();
    }

    /**
     * Configure the command. Set the required argument, help and description for current command.
     */
    protected function configure()
    {
        $this
            ->setName(self::COMMAND_NAME)
            ->setDescription('This command is used to generate bob Cache Billing.')
            ->addArgument('csvFile', InputArgument::REQUIRED, 'The csv file to be processed.')
            ->setHelp('
                The <info>billing</info> command will generate bills for Bob cache customers.

                <comment>Samples:</comment>
                  To generate bill using test.csv
                    <info>./bob billing test.csv</info>

                  * Please note that the user should have rights to read the csv file you specify.*
            ');
    }

    /**
     * Execute the command
     *
     * @param InputInterface $input Input Interface
     * @param OutputInterface $output Output Interface
     *
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        //Check if csv file given exists.
        if (!file_exists($csvFile = $input->getArgument('csvFile'))) {
            throw new \InvalidArgumentException(sprintf('The csvFile file "%s" does not exist or not readable.', $csvFile));
        }

        //Read CSV
        $this->csvReader->read($csvFile);
        $pageImpressions = $this->csvReader->getPageImpressions();
        $domainName      = $this->csvReader->getDomainName();

        //If csv is empty then, there is no need to ask user input
        if (empty($pageImpressions) || empty($domainName)) {
            echo 'CSV provided does not contain required data.
                Required fields: Page impressions and domain name.';
            exit;
        }

        //Ask user input for bill generation
        $userInputs = $this->askUserInputs($input, $output);

        //Template for existing customer is placed as "_template.odt" file in directory where customer bills are saved.
        $billsaveLocation = BILLS_LOCATION . $domainName . DIRECTORY_SEPARATOR;
        $templateToBeUsed = $billsaveLocation . self::EXISTING_CUSTOMER_BILL_TEMPLATE;

        //If the bill doesn't exists already, it means its a new customer
        if (!file_exists($billsaveLocation)) {
            $newCustomer = $this->getNewCustomer($input, $output);
            $templateToBeUsed = TEMPLATES_LOCATION . self::NEW_CUSTOMER_BILL_TEMPLATE;

            $this->billGenerator->setIsNewCustomer(true);
            $output->writeln("<fg=green>Generating bill for new customer " . $domainName);
        } else {
            $output->writeln("<fg=green>Generating bill for existing customer " . $domainName);
        }

        $this->billGenerator->setBillSaveLocation($billsaveLocation);
        $this->billGenerator->setTemplateToBeUsed($templateToBeUsed);

        //Generate Bill based upon user input
        $generatedBill = $this->billGenerator->generateBill(
            $pageImpressions,
            $domainName,
            $userInputs['unitType'],
            $userInputs['invoiceWithoutVat'],
            !empty($newCustomer) ? $newCustomer : null
        );

        $output->writeln("<fg=green>Bill has been generated at " . $generatedBill);
        exit;
    }

    /**
     * Ask question from user about subscription type
     *
     * @param InputInterface $input Input Interface
     * @param OutputInterface $output Output Interface
     *
     * @return mixed
     */
    protected function askUserInputs(InputInterface $input, OutputInterface $output)
    {
        /** @var QuestionHelper $helper */
        $helper = $this->getHelper('question');
        $unitTypeQuestion = (new ChoiceQuestion(
            'Please select customer Subscription type (default: pro)  ',
            $this->subscriptionTypes,
            1
        ))->setErrorMessage('Subscription type %s is invalid.');

        $invoiceWithoutVatQuestion = new ConfirmationQuestion(
            'By default the vat is included, do you want invoice without vat? (y/n)',
            false
        );

        $unitType = $helper->ask($input, $output, $unitTypeQuestion);
        $invoiceWithoutVat = $helper->ask($input, $output, $invoiceWithoutVatQuestion);

        if (true === $invoiceWithoutVat) {
            $output->writeln('<fg=green>Info: Generating bill without Vat!');
        }

        $userInputs = array();

        $userInputs['unitType'] = $unitType;
        $userInputs['invoiceWithoutVat'] = $invoiceWithoutVat;

        return $userInputs;
    }

    /**
     * We ask these questions if its a new customer.
     *
     * @param InputInterface $input Input Interface
     * @param OutputInterface $output Output Interface
     *
     * @return mixed
     */
    protected function getNewCustomer(InputInterface $input, OutputInterface $output)
    {
        /** @var QuestionHelper $helper */
        $helper = $this->getHelper('question');

        $customerNameQuestion = new CustomerQuestion('Please enter customer\'s or company\'s name for which the bill should be generated: ');
        $customerAddressQuestion = new CustomerQuestion('Please enter customer\'s street address with street number: ');
        $customerCityQuestion = new CustomerQuestion('Please enter the customer\'s city: ');
        $customerPostalCode = new CustomerQuestion('Please enter the customer\'s Postal Code (PLZ) : ');
        $customerStateQuestoin = new CustomerQuestion('Please enter the customer\'s state: ');
        $customerCountryQuestion = new CustomerQuestion('Please enter the customer\'s country: ');

        return (new Customer())
            ->setName($helper->ask($input, $output, $customerNameQuestion))
            ->setStreetAddress($helper->ask($input, $output, $customerAddressQuestion))
            ->setCity($helper->ask($input, $output, $customerCityQuestion))
            ->setPostalCode($helper->ask($input, $output, $customerPostalCode))
            ->setState($helper->ask($input, $output, $customerStateQuestoin))
            ->setCountry($helper->ask($input, $output, $customerCountryQuestion));
    }
}
