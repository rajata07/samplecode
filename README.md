# Bob Cache

Sample application which can generate bills using odt template with given csv data

## Getting Started

### Setup
Run the following command to install all the required dependencies:
```
php composer.phar install
```

### Commands
Execute the command to start bill generation

```
./app/console billing ./sampleCache.csv
```