<?php
/**
 * Csv reader
 *
 * @author Rajat Arora <rajata07@gmail.com>
 */
namespace BobCache\Services;

class CsvReader
{
    protected $csvParsingOptions = array(
        'ignoreFirstLine' => true
    );

    protected $pageImpressions = array();

    protected $domainName;

    const DEFAULT_DELIMITER = ';';

    /**
     * @return array
     */
    public function getPageImpressions()
    {
        return $this->pageImpressions;
    }

    /**
     * @return mixed
     */
    public function getDomainName()
    {
        return $this->domainName;
    }

    public function read($filename, $delimiter = self::DEFAULT_DELIMITER)
    {
        if (!file_exists($filename) || !is_readable($filename)) {
            return FALSE;
        }

        $ignoreFirstLine = $this->csvParsingOptions['ignoreFirstLine'];

        if (($handle = fopen($filename, 'r')) !== FALSE) {
            $i = 0;
            while (($data = fgetcsv($handle, 1000, $delimiter)) !== FALSE) {
                $i++;
                if ($ignoreFirstLine && $i == 1) {
                    continue;
                }
                //Read rows only if page impression not empty in our case.
                if (!empty($data[2])) {
                    $this->pageImpressions[] = $data[2];
                }
                if (empty($this->domainName)) {
                    $this->domainName = $data[1];
                }
            }
            fclose($handle);
        }
        return true;
    }
}