<?php

date_default_timezone_set('Europe/Berlin');

define('ROOT_DIRECTORY', realpath(__DIR__ . '/..'));

// include the composer auto-loader
require_once ROOT_DIRECTORY . '/vendor/autoload.php';