<?php

namespace BobCache\Models;

/**
 * CustomerBill Attributes.
 *
 * @author Rajat Arora <rajata07@gmail.com>
 */
class CustomerBill
{
    /**
     * @var Integer
     */
    protected $unitPrice;

    /**
     * @var Integer
     */
    protected $numberOfUnitsUsed;

    /**
     * @var String
     */
    protected $invoiceNumber;

    /**
     * @var Integer
     */
    protected $pageImpressions;

    /**
     * @var float
     */
    protected $amount;

    /**
     * @var float
     */
    protected $vat;

    /**
     * @var float
     */
    protected $totalAmount;

    /**
     * @var string
     */
    protected $billingCycle;

    /**
     * @var string
     */
    protected $billDate;

    /**
     * @var string
     */
    protected $domainName;

    /**
     * @var boolean
     */
    protected $excludeVat;

    /**
     * @return boolean
     */
    public function isExcludeVat()
    {
        return $this->excludeVat;
    }

    /**
     * @param boolean $excludeVat
     */
    public function setExcludeVat($excludeVat)
    {
        $this->excludeVat = $excludeVat;
    }

    /**
     * @return int
     */
    public function getUnitPrice()
    {
        return $this->unitPrice;
    }

    /**
     * @return String
     */
    public function getInvoiceNumber()
    {
        return $this->invoiceNumber;
    }

    /**
     * @param String $invoiceNumber
     */
    public function setInvoiceNumber($invoiceNumber)
    {
        $this->invoiceNumber = (string) $invoiceNumber;
    }

    /**
     * @return string
     */
    public function getBillingCycle()
    {
        return $this->billingCycle;
    }

    /**
     * @param string $billingCycle
     */
    public function setBillingCycle($billingCycle)
    {
        $this->billingCycle = (string) $billingCycle;
    }

    /**
     * @return string
     */
    public function getDomainName()
    {
        return $this->domainName;
    }

    /**
     * @param string $domainName
     */
    public function setDomainName($domainName)
    {
        $this->domainName = (string) $domainName;
    }

    /**
     * @param int $unitPrice
     */
    public function setUnitPrice($unitPrice)
    {
        $this->unitPrice = (int) $unitPrice;
    }

    /**
     * @return int
     */
    public function getNumberOfUnitsUsed()
    {
        return $this->numberOfUnitsUsed;
    }

    /**
     * @param int $numberOfUnitsUsed
     */
    public function setNumberOfUnitsUsed($numberOfUnitsUsed)
    {
        $this->numberOfUnitsUsed = (int) $numberOfUnitsUsed;
    }

    /**
     * @return int
     */
    public function getPageImpressions()
    {
        return $this->pageImpressions;
    }

    /**
     * @param int $pageImpressions
     */
    public function setPageImpressions($pageImpressions)
    {
        $this->pageImpressions = (int) $pageImpressions;
    }

    /**
     * @return float
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param float $amount
     */
    public function setAmount($amount)
    {
        $this->amount = (float) $amount;
    }

    /**
     * @return float
     */
    public function getVat()
    {
        if ($this->excludeVat) {
            return '';
        }
        return $this->vat;
    }

    /**
     * @param float $vat
     */
    public function setVat($vat)
    {
        $this->vat = (float) $vat;
    }

    /**
     * @return float
     */
    public function getTotalAmount()
    {
        return $this->totalAmount;
    }

    /**
     * @param float $totalAmount
     */
    public function setTotalAmount($totalAmount)
    {
        $this->totalAmount = (float) $totalAmount;
    }

    /**
     * @return string
     */
    public function getBillDate()
    {
        return $this->billDate;
    }

    /**
     * @param string $billDate
     */
    public function setBillDate($billDate)
    {
        $this->billDate = $billDate;
    }
}
