<?php

$rootPath = realpath(__DIR__ . '/../..');

/**
 * Root directory of the project
 */
define('ROOT_DIRECTORY', $rootPath . DIRECTORY_SEPARATOR);

/**
 * Template directory path
 */
define('TEMPLATES_LOCATION', $rootPath . '/src/BobCache/Resources/templates/');

/**
 * Path where all the bills are saved.
 */
define('BILLS_LOCATION', $rootPath . '/generatedBills/');
