<?php

namespace BobCache\Question;

use Symfony\Component\Console\Question\Question;

class CustomerQuestion extends Question
{
    public function __construct($question, $default = null)
    {
        parent::__construct($question, $default);
        $this->setValidator(function ($answer) {
            if (empty($answer)) {
                throw new \RuntimeException(
                    'This attribute of the customer should not be empty'
                );
            }
            return $answer;
        });
        $this->setMaxAttempts(3);
    }
}
