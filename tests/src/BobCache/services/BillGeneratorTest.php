<?php

use MBence\OpenTBSBundle\Services\OpenTBS;
use BobCache\Services\BillGenerator;

class BillGeneratorTest extends PHPUnit_Framework_TestCase
{
    /**
     * Test the method which provides the customer bill based upon PI's and subscription (unit) type
     */
    public function testGetCustomerBill()
    {
        $mockedOpenTbsService = new OpenTBS();
        $billGeneratorService = new BillGenerator($mockedOpenTbsService);

        $sampleDomain = 'www.masterkunde.de';
        $pageImpressions = array('100', '100000');

        //case with pro unit type including vat.
        /** @var \BobCache\Models\CustomerBill $customerBill */
        $customerBill = $this->invokeMethod(
            $billGeneratorService,
            'getCustomerBill',
            array($pageImpressions, $sampleDomain, 'pro', false)
        );

        $this->assertInstanceOf('BobCache\Models\CustomerBill', $customerBill);

        $this->assertEquals(2, $customerBill->getNumberOfUnitsUsed());
        $this->assertEquals(240, $customerBill->getAmount());
        $this->assertEquals(120, $customerBill->getUnitPrice());
        $this->assertEquals($sampleDomain, $customerBill->getDomainName());
        $this->assertEquals(45.60, $customerBill->getVat());
        $this->assertEquals(285.60, $customerBill->getTotalAmount());

        //Case for basic unit type with zero vat.
        /** @var \BobCache\Models\CustomerBill $customerBill */
        $customerBill = $this->invokeMethod(
            $billGeneratorService,
            'getCustomerBill',
            array($pageImpressions, $sampleDomain, 'basic', true)
        );

        $this->assertInstanceOf('BobCache\Models\CustomerBill', $customerBill);

        $this->assertEquals(2, $customerBill->getNumberOfUnitsUsed());
        $this->assertEquals(100, $customerBill->getAmount());
        $this->assertEquals(50, $customerBill->getUnitPrice());
        $this->assertEquals($sampleDomain, $customerBill->getDomainName());
        $this->assertEmpty($customerBill->getVat());
        $this->assertEquals(100, $customerBill->getTotalAmount());
    }

    /**
     * Call protected/private method of a class.
     *
     * @param object &$object    Instantiated object that we will run method on.
     * @param string $methodName Method name to call
     * @param array  $parameters Array of parameters to pass into method.
     *
     * @return mixed Method return.
     */
    protected function invokeMethod(&$object, $methodName, array $parameters = array())
    {
        $reflection = new \ReflectionClass(get_class($object));
        $method = $reflection->getMethod($methodName);
        $method->setAccessible(true);

        return $method->invokeArgs($object, $parameters);
    }
}
