<?php
/**
 * Bill calcualtor which sums up the Page impressions to calcualte the bill.
 *
 * @author Rajat Arora <rajata07@gmail.com>
 */
namespace BobCache\Services;

use BobCache\Commands\BillingCommand;
use BobCache\Models\Customer;
use BobCache\Models\CustomerBill;
use MBence\OpenTBSBundle\Services\OpenTBS;

class BillGenerator
{
    const PAGE_IMPRESSION_UNIT_LIMIT = 100000;

    const VAT = 0.19;

    const VAT_TEXT_LINE = '19% MwSt.';

    const TEMPLATE_PLACEHOLDER_CHARACTER = '%';

    const BILL_NAME = 'bill_%s.odt';

    const INVOICE_FORMAT = '%s-M%s';

    /**
     * Location of Existing bill of the customer
     * @var string
     */
    protected $templateToBeUsed;

    /**
     * Location where the bill should be saved.
     * @var string
     */
    protected $billSaveLocation;

    /**
     * Incdicates whether it's a new customer.
     * @var bool
     */
    protected $isNewCustomer = false;

    /**
     * Plugin used for template generation.
     *
     * @var OpenTBS
     */
    protected $openTbs;

    /**
     * Prices for different types of unit.
     * @var array
     */
    protected $unitPrices = array('basic' => 50, 'pro' => 120);

    /**
     * BillGenerator constructor.
     * @param OpenTBS $openTbs
     */
    public function __construct(OpenTBS $openTbs)
    {
        $this->openTbs = $openTbs;

        $this->openTbs->SetOption('chr_open', self::TEMPLATE_PLACEHOLDER_CHARACTER);
        $this->openTbs->SetOption('chr_close', self::TEMPLATE_PLACEHOLDER_CHARACTER);
    }

    /**
     * Calculates the bill and generate the odt file.
     *
     * @param array         $pageImpressions  Array containing all PI's
     * @param string        $domainName       Domain name for which the bill should be generated
     * @param string        $subscriptionType Subscription type of customer
     * @param boolean       $excludeVat       Exclude vat flag
     * @param Customer|null $customer         Customer Object
     *
     * @return string
     */
    public function generateBill($pageImpressions, $domainName, $subscriptionType, $excludeVat, $customer)
    {
        //Get Customer bill based upon given attributes
        $customerBill = $this->getCustomerBill(
            $pageImpressions,
            $domainName,
            $subscriptionType,
            $excludeVat
        );

        $this->ensureDirectoryExists($this->billSaveLocation);

        $finalBill = $this->billSaveLocation . sprintf(self::BILL_NAME, date('Y_m'));

        //Load Template and replace placeholders
        $this->openTbs->LoadTemplate($this->templateToBeUsed . '#content.xml', OPENTBS_ALREADY_UTF8);
        $this->replacePlaceHolders($customer, $customerBill);
        $this->openTbs->Show(OPENTBS_FILE, $finalBill);

        return $finalBill;
    }

    /**
     * @param Customer     $customer     Customer Object
     * @param CustomerBill $customerBill Customer bill object
     */
    protected function replacePlaceHolders($customer, $customerBill)
    {
        if (!empty($customer) && $this->isNewCustomer) {
            $this->openTbs->MergeField('ANSCHRIFT-ZEILE1', $customer->getName());
            $this->openTbs->MergeField('ANSCHRIFT-ZEILE2', $customer->getStreetAddress());
            $this->openTbs->MergeField('ANSCHRIFT-ZEILE3', $customer->getPostalCode() . ' ' . $customer->getCity());
            $this->openTbs->MergeField('ANSCHRIFT-ZEILE4', $customer->getState() . ', ' . $customer->getCountry());

            //Save template before replacing address fields.
            $saveTemplateWithCustomerAddress = $this->billSaveLocation . BillingCommand::EXISTING_CUSTOMER_BILL_TEMPLATE;
            if (!file_exists($saveTemplateWithCustomerAddress)) {
                $handler = fopen($saveTemplateWithCustomerAddress, "w");
                fclose($handler);
            }
            //Save template with replaced placeholders.
            $this->openTbs->Show(OPENTBS_FILE, $saveTemplateWithCustomerAddress);
            $this->openTbs->LoadTemplate($saveTemplateWithCustomerAddress . '#content.xml', OPENTBS_ALREADY_UTF8);
        }

        if ($customerBill->isExcludeVat()) {
            $this->openTbs->Source = str_replace(self::VAT_TEXT_LINE, '', $this->openTbs->Source);
        }

        $this->openTbs->MergeField('DOMAIN', $customerBill->getDomainName());
        $this->openTbs->MergeField('ANZAHL', $customerBill->getNumberOfUnitsUsed());
        $this->openTbs->MergeField('EINHEITSPREIS', $customerBill->getUnitPrice());
        $this->openTbs->MergeField('NETTOMYRA', $customerBill->getAmount());
        $this->openTbs->MergeField('MWST', $customerBill->getVat());
        $this->openTbs->MergeField('GESAMTSUMME', $customerBill->getTotalAmount());
        $this->openTbs->MergeField('REDATUM', $customerBill->getBillDate());
        $this->openTbs->MergeField('RENR', $customerBill->getInvoiceNumber());
        $this->openTbs->MergeField('MONAT', $customerBill->getBillingCycle());
    }

    /**
     * Calculate the bill based upon Subscription Type
     *
     * @param array   $pageImpressions  Array containing all PI's
     * @param string  $domainName       Domain name for which the bill should be generated
     * @param string  $subscriptionType Subscription type of customer
     * @param boolean $excludeVat       Indicates whether to exclude Vat or not
     * @return CustomerBill
     */
    protected function getCustomerBill($pageImpressions, $domainName, $subscriptionType, $excludeVat)
    {
        $totalPageImpressions = array_sum($pageImpressions);
        $unitPrice  = $this->unitPrices[$subscriptionType];
        $unitsUsed  = ceil($totalPageImpressions / self::PAGE_IMPRESSION_UNIT_LIMIT);
        $billAmount = $unitsUsed * $unitPrice;
        $vatAmount = 0;
        if (!$excludeVat) {
            $vatAmount  = $billAmount * self::VAT;
        }

        $totalAmount = $billAmount + $vatAmount;
        list($day, $month, $year) = explode(".", date('j.n.Y'));

        $customerBill = new CustomerBill();
        $customerBill->setUnitPrice($unitPrice);
        $customerBill->setNumberOfUnitsUsed($unitsUsed);
        $customerBill->setAmount(number_format($billAmount, 2));
        $customerBill->setVat(number_format($vatAmount, 2));
        $customerBill->setTotalAmount(number_format($totalAmount, 2));
        $customerBill->setDomainName($domainName);
        $customerBill->setInvoiceNumber(sprintf(self::INVOICE_FORMAT, $year, $month . $day));
        $customerBill->setBillingCycle(date('F Y'));
        $customerBill->setBillDate(date('d.m.Y'));
        $customerBill->setExcludeVat($excludeVat);

        return $customerBill;
    }

    /**
     * @param $directory
     */
    protected function ensureDirectoryExists($directory)
    {
        if (!file_exists($directory)) {
            mkdir($directory, 0777, true);
        }
    }

    /**
     * @param string $templateToBeUsed
     */
    public function setTemplateToBeUsed($templateToBeUsed)
    {
        $this->templateToBeUsed = $templateToBeUsed;
    }

    /**
     * @param boolean $isNewCustomer
     */
    public function setIsNewCustomer($isNewCustomer)
    {
        $this->isNewCustomer = $isNewCustomer;
    }

    /**
     * @param string $billSaveLocation
     */
    public function setBillSaveLocation($billSaveLocation)
    {
        $this->billSaveLocation = $billSaveLocation;
    }
}
