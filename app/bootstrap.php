<?php

date_default_timezone_set('Europe/Berlin');

// include the composer auto-loader
require_once __DIR__ . '/../vendor/autoload.php';

// include the applicaiton config
require_once(__DIR__ . '/config/config.php');

