<?php

namespace BobCache\Models;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * Customer Attributes.
 *
 * @author Rajat Arora <rajata07@gmail.com>
 */
class Customer
{
    /**
     * @var String
     */
    protected $name;

    /**
     * @var String
     */
    protected $streetAddress;

    /**
     * @var String
     */
    protected $city;

    /**
     * @var String
     */
    protected $state;

    /**
     * @var String
     */
    protected $postalCode;

    /**
     * @var String
     */
    protected $country;

    /**
     * @return String
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return String
     */
    public function getStreetAddress()
    {
        return $this->streetAddress;
    }

    /**
     * @param $streetAddress
     * @return $this
     */
    public function setStreetAddress($streetAddress)
    {
        $this->streetAddress = $streetAddress;
        return $this;
    }

    /**
     * @return String
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param $city
     * @return $this
     */
    public function setCity($city)
    {
        $this->city = $city;
        return $this;
    }

    /**
     * @return String
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @param $state
     * @return $this
     */
    public function setState($state)
    {
        $this->state = $state;
        return $this;
    }

    /**
     * @return String
     */
    public function getPostalCode()
    {
        return $this->postalCode;
    }

    /**
     * @param $postalCode
     * @return $this
     */
    public function setPostalCode($postalCode)
    {
        $this->postalCode = $postalCode;
        return $this;
    }

    /**
     * @return String
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param $country
     * @return $this
     */
    public function setCountry($country)
    {
        $this->country = $country;
        return $this;
    }
}
